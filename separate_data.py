import random
import sys
import os

f1 = open(sys.argv[1], encoding='utf-8')
f2 = open(sys.argv[2], encoding='utf-8')
dev = float(sys.argv[3])
test = float(sys.argv[4])

data = list(zip(f1.readlines(), f2.readlines()))
random.shuffle(data)

train_len = int(len(data) * (1-dev-test))
dev_len = int(len(data) * dev)
test_len = int(len(data) * test)

data_train = data[:train_len]
data_dev = data[train_len:-test_len]
data_test = data[-test_len:]

print(len(data_train), len(data_dev), len(data_test))

dir1, fname1 = os.path.split(sys.argv[1])
dir2, fname2 = os.path.split(sys.argv[2])

fname1 = fname1.split('.')
fname1 = ['.'.join(fname1[:-1]), fname1[-1]]

fname2 = fname2.split('.')
fname2 = ['.'.join(fname2[:-1]), fname2[-1]]

open(dir1 + '/' + fname1[0] + '_train.' + fname1[1], 'w', encoding='utf-8').write(''.join([d[0] for d in data_train]))
open(dir2 + '/' + fname2[0] + '_train.' + fname2[1], 'w', encoding='utf-8').write(''.join([d[1] for d in data_train]))
open(dir1 + '/' + fname1[0] + '_dev.' + fname1[1], 'w', encoding='utf-8').write(''.join([d[0] for d in data_dev]))
open(dir2 + '/' + fname2[0] + '_dev.' + fname2[1], 'w', encoding='utf-8').write(''.join([d[1] for d in data_dev]))
open(dir1 + '/' + fname1[0] + '_test.' + fname1[1], 'w', encoding='utf-8').write(''.join([d[0] for d in data_test]))
open(dir2 + '/' + fname2[0] + '_test.' + fname2[1], 'w', encoding='utf-8').write(''.join([d[1] for d in data_test]))
