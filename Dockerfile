FROM moxel/py3-pytorch-cpu

RUN pip3 install --upgrade pip
RUN pip3 install six tqdm future torchtext==0.1.1 nltk flask flask_cors

ADD . /code
WORKDIR /code
CMD ["python3", "api.py"]